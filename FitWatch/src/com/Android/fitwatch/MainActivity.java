package com.Android.fitwatch;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.widget.ArrayAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	ProgressDialog dialog;
	Button pebbleBtn;
	Button fitbitBtn;
	Button subFitBitBtn;
	TextView steps;
	TextView distance;
	TextView cals;
	TextView floors;
	TextView lightMin;
	TextView FairMin;
	TextView VeryMin;
	Context context;
	SharedPreferences appPreferences;
	boolean elevationDevice;
	private OAuthService oService;
	static String userEncodedID;
	
	static final int REQUEST_ENABLE_BT = 1;
	static final int REQUEST_DISCOVERABLE_BT = 0;
	BluetoothAdapter bluetoothAdapter;
	Button disableBT;
	Button enableBT;
	Button backToMain;
	Button bluetoothBtn;
	Button search;
	Button list;
	TextView btStatusText;
	Set<BluetoothDevice> pairedDeviceList;
	ListView listView;
	ArrayAdapter<String> BTArrayAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		appPreferences = getSharedPreferences("Preferences", 0);
		
		//Added 3/21/2014 Test
		System.out.print(appPreferences.getString("RawAccess", ""));
		String RawToken=appPreferences.getString("RawAccess", "");
		userEncodedID=RawToken.substring(RawToken.lastIndexOf("=")+1,RawToken.length());
		System.out.print("\n User Ecoded ID"+userEncodedID);
		
		oService = new ServiceBuilder().provider(FitbitApi.class)
				.apiKey(getString(R.string.CONSUMER_KEY))
				.apiSecret(getString(R.string.CONSUMER_SECRET))
				.callback(getString(R.string.CALLBACK)).build();
				
		if (appPreferences.getString("DeviceType", "").equals("One")  
				|| appPreferences.getString("DeviceType", "").equals("Force"))
			elevationDevice = true;
		else
			elevationDevice = false;

		if (elevationDevice)
			setContentView(R.layout.force_one);
		else
			setContentView(R.layout.flex_zip);

		steps = (TextView) findViewById(R.id.steps);
		distance = (TextView) findViewById(R.id.distance);
		cals = (TextView) findViewById(R.id.cals);
		floors = (TextView) findViewById(R.id.floors);
		lightMin = (TextView) findViewById(R.id.lightActiveMin);
		FairMin = (TextView) findViewById(R.id.fairlyActiveMin);
		VeryMin = (TextView) findViewById(R.id.veryActiveMin);
		fitbitBtn = (Button) findViewById(R.id.fitbitBtn);
		pebbleBtn = (Button) findViewById(R.id.pebbleBtn);	
		subFitBitBtn=(Button) findViewById(R.id.subFitBitBtn);		
		disableBT=(Button) findViewById(R.id.disableBluetooth);
		enableBT=(Button) findViewById(R.id.enableBluetooth);
		bluetoothBtn=(Button) findViewById(R.id.bluetoothBtn);
		backToMain=(Button) findViewById(R.id.backToMain);
		search=(Button) findViewById(R.id.search);
		list=(Button) findViewById(R.id.listPairedDevices);
		bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();			
		refreshStats();

		context = this;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	// Sync with FitBit
	public void SyncFitbit(View view) {

		view.setEnabled(false);
		
		Token accessToken = new Token(appPreferences.getString(
				"AccessToken", ""), appPreferences.getString(
				"AccessSecret", ""), appPreferences.getString(
				"RawAccess", ""));

		sendRequest(accessToken);
		
		(new AsyncTask<Void, Void, Void>() {

			@Override
			protected void onPreExecute() {
				dialog = new ProgressDialog(context);
				dialog.setTitle("Syncing with FitBit");
				dialog.setMessage("Please wait.");
				dialog.setCancelable(false);
				dialog.setIndeterminate(true);
				dialog.show();
			}

			@Override
			protected Void doInBackground(Void... arg0) {

				try { Thread.sleep(3000);} 
				catch (InterruptedException e) {}
				
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
	
				dialog.dismiss();
				fitbitBtn.setEnabled(true);
				refreshStats();

			}
		}).execute();
	}

	//Connect to Pebble through Bluetooth
	public void ConnectToPebble(View view)
	{		
		view.setEnabled(true);
		setContentView(R.layout.bluetooth_menu);			
		btStatusText = (TextView) findViewById(R.id.btStatusText);
	
		if (bluetoothAdapter == null)    // The device does not support bluetooth
		{	
		//disableBT.setEnabled(false);
		//enableBT.setEnabled(false);
		btStatusText.setText("Bluetooth not supported");
		
			Toast.makeText(getApplicationContext(), "Your device does not support Bluetooth", Toast.LENGTH_LONG).show();
		}		
		else
		{		
		if (bluetoothAdapter.isEnabled())
		{
				btStatusText.setText("Bluetooth is enabled");
		}
			else
			{
				btStatusText.setText("Bluetooth is not enabled");
			}
		}	
	}
	
	public void ReloadMainPage(View view)
	{
		view.setEnabled(true);
		setContentView(R.layout.flex_zip);
	}
	
	public void EnableBluetooth(View view)
	{
		
		if (!bluetoothAdapter.isEnabled())
		{
			Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
			
			Toast.makeText(getApplicationContext(), "Bluetooth is now enabled", Toast.LENGTH_LONG).show();
		}
		else
		{
			Toast.makeText(getApplicationContext(), "Bluetooth is already enabled", Toast.LENGTH_SHORT).show();
		}
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent)
	{
		btStatusText = (TextView) findViewById(R.id.btStatusText);
		if (requestCode == REQUEST_ENABLE_BT)
		{
			btStatusText.setText("Bluetooth is enabled");
		}
		else
		{
			btStatusText.setText("Bluetooth is not enabled");
		}
	}
	
	
	public void DisableBluetooth(View view)
	{
		if (bluetoothAdapter.isEnabled())
		{
		bluetoothAdapter.disable();
		btStatusText.setText("Bluetooth is not enabled");
		Toast.makeText(getApplicationContext(), "Bluetooth has been disabled", Toast.LENGTH_SHORT).show();
		}
		else
		{
			Toast.makeText(getApplicationContext(), "Bluetoth is already disabled", Toast.LENGTH_SHORT).show();
		}
		
	}
	
	
	public void ListPairedDevices(View view)
	{
		listView = (ListView)findViewById(R.id.mybtlist);
		BTArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
		listView.setAdapter(BTArrayAdapter);
		pairedDeviceList = bluetoothAdapter.getBondedDevices();
		for (BluetoothDevice device : pairedDeviceList)
			BTArrayAdapter.add(device.getName()+ "\n" + device.getAddress());
		
		Toast.makeText(getApplicationContext(), "Show Paired Devices", Toast.LENGTH_SHORT).show();
	}
	
	final BroadcastReceiver bReceiver = new BroadcastReceiver()
	{
		public void onReceive(Context context, Intent intent)
		{
			String action = intent.getAction();
			
			if (BluetoothDevice.ACTION_FOUND.equals(action))
			{
				BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				BTArrayAdapter.add(device.getName() + "\n" + device.getAddress());
				BTArrayAdapter.notifyDataSetChanged();
			}
		}
	};
	
	public void Search(View view)
	{
		listView = (ListView)findViewById(R.id.mybtlist);
		BTArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
		listView.setAdapter(BTArrayAdapter);
		if (bluetoothAdapter.isDiscovering())
		{
			bluetoothAdapter.cancelDiscovery();
		}
		else
		{
			BTArrayAdapter.clear();
			Intent discIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
			startActivityForResult(discIntent, REQUEST_DISCOVERABLE_BT);
			bluetoothAdapter.startDiscovery();
			registerReceiver(bReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));

			//Toast.makeText(getApplicationContext(), "Connected", Toast.LENGTH_SHORT).show();
		}
	}

	
	// Send Data to Pebble
	public void SyncPebble(View view) {

		/*
		 * if connected to Pebble do the follow
		 * else
		 * setcontent(R.layout.bluetoothMenu) and toast saying please enable bluetooth
		 */
		
		view.setEnabled(false);
				
		(new AsyncTask<Void, Void, Void>() {

			@Override
			protected void onPreExecute() {
				dialog = new ProgressDialog(context);
				dialog.setTitle("Syncing with Pebble");
				dialog.setMessage("Please wait.");
				dialog.setCancelable(false);
				dialog.setIndeterminate(true);
				dialog.show();
			}

			@Override
			protected Void doInBackground(Void... arg0) {

				try { Thread.sleep(3000);} 
				catch (InterruptedException e) {}
				
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
	
				dialog.dismiss();
				pebbleBtn.setEnabled(true);
				
			}
		}).execute();		
	}

	private void refreshStats() {

		steps.setText(appPreferences.getString("Steps", "0"));
		distance.setText(appPreferences.getString("Distance", "0") + " miles");
		cals.setText(appPreferences.getString("CaloriesOut", "0"));
		lightMin.setText(appPreferences.getString("LightlyActiveMinutes", "0"));
		FairMin.setText(appPreferences.getString("FairlyActiveMinutes", "0"));
		VeryMin.setText(appPreferences.getString("VeryActiveMinutes", "0"));

		if (elevationDevice)
			floors.setText(appPreferences.getString("FloorsClimbed", "0"));

	}	
	
	private void sendRequest(final Token accessToken) {
		(new AsyncTask<Void, Void, Response>() {
			@Override
			protected Response doInBackground(Void... params) {
				
				OAuthRequest request = getCurrentActivities();
				oService.signRequest(accessToken, request);
				Response response = request.send();
				System.out.print("\nJSON:" + response.getBody());
				return response;
			}

			@Override
			protected void onPostExecute(final Response response) {

				parseJSON(response);
			}

		}).execute();
	}

	private  void parseJSON(Response response) {

		try {
			
			JSONObject fullObject = new JSONObject(response.getBody());
			JSONObject summary = fullObject.getJSONObject("summary");
			JSONArray distances = summary.getJSONArray("distances");
			JSONObject totaldistance = distances.getJSONObject(4);

			SharedPreferences.Editor editor = appPreferences.edit();

			editor.putString("Steps", summary.getString("steps"));
			editor.putString("Distance", totaldistance.getString("distance"));
			editor.putString("CaloriesOut", summary.getString("caloriesOut"));
			editor.putString("LightlyActiveMinutes", summary.getString("lightlyActiveMinutes"));
			editor.putString("FairlyActiveMinutes", summary.getString("fairlyActiveMinutes"));
			editor.putString("VeryActiveMinutes", summary.getString("veryActiveMinutes"));

			if (elevationDevice) 
				editor.putString("FloorsClimbed", summary.getString("floors"));
			
			editor.commit();

		} catch (JSONException e) {
			e.printStackTrace();
		}		
	}
	
	private static OAuthRequest getCurrentActivities() {

		String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

		return new OAuthRequest(Verb.GET,
				"http://api.fitbit.com/1/user/-/activities/date/" + date + ".json");
	}
	
	
	
	public void subscribeFitBit(View view)
	{
		view.setEnabled(false);
		
		Token accessToken = new Token(appPreferences.getString(
				"AccessToken", ""), appPreferences.getString(
				"AccessSecret", ""), appPreferences.getString(
				"RawAccess", ""));

		sendSubcribeRequest(accessToken);
		
		(new AsyncTask<Void, Void, Void>() {

			@Override
			protected void onPreExecute() {
				dialog = new ProgressDialog(context);
				dialog.setTitle("Subscribing to  FitBit");
				dialog.setMessage("Please wait.");
				dialog.setCancelable(false);
				dialog.setIndeterminate(true);
				dialog.show();
			}

			@Override
			protected Void doInBackground(Void... arg0) {

				try { Thread.sleep(3000);} 
				catch (InterruptedException e) {}
				
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
	
				dialog.dismiss();
				subFitBitBtn.setEnabled(false);
				refreshStats();

			}
		}).execute();
	}
	
	private void sendSubcribeRequest(final Token accessToken) {
		(new AsyncTask<Void, Void, Response>() {
			@Override
			protected Response doInBackground(Void... params) {
				
				OAuthRequest request = subscribeUserUpdates();
				oService.signRequest(accessToken, request);
				Response response = request.send();
				/* System.out.print("\nJSON: The Message "+response.getMessage()+ " \n The Body of the Message " + response.getBody() + "\n 7" +
						"\n Response to String"+ response.toString()+" UserId " + userEncodedID + "\n The HEADERS" +response.getHeaders());
				*/
				
				String responseString=response.toString(); 
				String subID=responseString.substring(responseString.lastIndexOf("@")+1,responseString.length());
				/*  Used for Testing System.out.print("\n subID : "+subID);
				*/
				return response;
			}

			@Override
			protected void onPostExecute(final Response response) {

		
			}
			

		}).execute();
	}
	
	private static OAuthRequest subscribeUserUpdates(){
		
		//String urlR="http://api.fitbit.com/1/user/"+ UserId +"/foods/apiSubscriptions/"+UserId +"-foods.json";
		//System.out.print(urlR); 
		//http://api.fitbit.com/1/user/-/apiSubscriptions/1.json
		return new OAuthRequest(Verb.POST,"http://api.fitbit.com/1/user/"+ userEncodedID +"/apiSubscriptions.json");
	}
	
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		unregisterReceiver(bReceiver);
	}
	
}
