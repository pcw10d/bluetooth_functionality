package com.Android.fitwatch;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;



public class Splash extends Activity {
	
	private WebView loginWebView;
	private OAuthService oService;
	private Token requestToken;
	static SharedPreferences appPreferences;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.splash);
		
		appPreferences = getSharedPreferences("Preferences", 0);
		
		oService = new ServiceBuilder().provider(FitbitApi.class)
				.apiKey(getString(R.string.CONSUMER_KEY))
				.apiSecret(getString(R.string.CONSUMER_SECRET))
				.callback(getString(R.string.CALLBACK)).build();

		loginWebView = (WebView) findViewById(R.id.webview);
		loginWebView.clearCache(true);
		loginWebView.getSettings().setJavaScriptEnabled(true);
		loginWebView.getSettings().setBuiltInZoomControls(true);
		loginWebView.getSettings().setDisplayZoomControls(false);
		loginWebView.setWebViewClient(mWebViewClient);

		if (!appPreferences.contains("AccessToken"))
			authorizeApp();
		else {

			Token accessToken = new Token(appPreferences.getString(
					"AccessToken", ""), appPreferences.getString(
					"AccessSecret", ""), appPreferences.getString(
					"RawAccess", ""));

			sendRequest(accessToken);
		}
	}

	
	//Gets authorization url using request token then loads url.
	private void authorizeApp() {
		(new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				requestToken = oService.getRequestToken();
				return oService.getAuthorizationUrl(requestToken);
			}

			@Override
			protected void onPostExecute(String url) {

				
				loginWebView.setVisibility(View.VISIBLE);
				loginWebView.loadUrl(url);
			}
		}).execute();
	}

	//Custom WebViewClient that waits for the callback url to be returned and extracts the verifier.
	private WebViewClient mWebViewClient = new WebViewClient() {
		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			if ((url != null) && (url.startsWith(getString(R.string.CALLBACK)))) {

				loginWebView.stopLoading();
				loginWebView.setVisibility(View.INVISIBLE);
				Uri uri = Uri.parse(url);
				final Verifier verifier = new Verifier(
						uri.getQueryParameter("oauth_verifier"));
				getAccess(verifier);
			} else {
				super.onPageStarted(view, url, favicon);
			}
		}
	};

	//Gets the access token and then saves it into the App preference file.
	private void getAccess(final Verifier verifier) {
		(new AsyncTask<Void, Void, Token>() {
			@Override
			protected Token doInBackground(Void... params) {
				return oService.getAccessToken(requestToken, verifier);
			}

			@Override
			protected void onPostExecute(Token accessToken) {

				SharedPreferences.Editor editor = appPreferences.edit();

				// save AccessToken
				editor.putString("AccessToken", accessToken.getToken());
				editor.putString("AccessSecret", accessToken.getSecret());
				editor.putString("RawAccess", accessToken.getRawResponse());
				editor.commit();

				
				
				sendRequest(accessToken);

			}
		}).execute();
	}

	//The Fitbit API request or requests are built and sent. The result is then passed on for parsing.
	void sendRequest(final Token accessToken) {
		(new AsyncTask<Void, Void, Response>() {
			@Override
			protected Response doInBackground(Void... params) {

				if (!appPreferences.contains("DeviceType")) {
					setCurrentDevice(accessToken);
				}

				OAuthRequest request = getCurrentActivities();
				oService.signRequest(accessToken, request);
				Response response = request.send();
				System.out.print("\nJSON:" + response.getBody());
				return response;
			}

			@Override
			protected void onPostExecute(final Response response) {

				parseJSON(response);
			}

		}).execute();
	}

	//The JSON object received in the call is parsed.  
	private void parseJSON(Response response) {

		try {
			JSONObject fullObject = new JSONObject(response.getBody());
			JSONObject summary = fullObject.getJSONObject("summary");
			JSONArray distances = summary.getJSONArray("distances");
			JSONObject totaldistance = distances.getJSONObject(1);
			
			DecimalFormat df = new DecimalFormat("####0.00");
	
			String distance = df.format(totaldistance.getDouble("distance") * 0.62137);
			
			SharedPreferences.Editor editor = appPreferences.edit();

			editor.putString("Steps", summary.getString("steps"));
			editor.putString("Distance", distance);
			editor.putString("CaloriesOut", summary.getString("caloriesOut"));
			editor.putString("LightlyActiveMinutes", summary.getString("lightlyActiveMinutes"));
			editor.putString("FairlyActiveMinutes", summary.getString("fairlyActiveMinutes"));
			editor.putString("VeryActiveMinutes", summary.getString("veryActiveMinutes"));
						
			if (appPreferences.getString("DeviceType", "").equals("One")  || 
					appPreferences.getString("DeviceType", "").equals("Force")) 

				editor.putString("FloorsClimbed", summary.getString("floors"));
			
			editor.commit();

		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		Intent intent = new Intent(Splash.this, MainActivity.class);

		Splash.this.startActivity(intent);
		Splash.this.finish();

	}

	//Builds a Fitbit API call using the current date.
	private static OAuthRequest getCurrentActivities() {

		String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

		return new OAuthRequest(Verb.GET, "http://api.fitbit.com/1/user/-/activities/date/" + date + ".json");

	}

	// Calls the Fitbit API to get the device type and saves it to the App preference file.
	private void setCurrentDevice(Token accessToken) {

		OAuthRequest request = new OAuthRequest(Verb.GET, "http://api.fitbit.com/1/user/-/devices.json");
		oService.signRequest(accessToken, request);
		Response response = request.send();
		System.out.print("\nJSON:" + response.getBody());

		try {
			JSONArray fullObject = new JSONArray(response.getBody());
			JSONObject deviceInfo = fullObject.getJSONObject(0);

			SharedPreferences.Editor editor = appPreferences.edit();

			editor.putString("DeviceType",
					deviceInfo.getString("deviceVersion"));
			editor.commit();

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}
}